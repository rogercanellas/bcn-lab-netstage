;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     midominio.com. root.midominio.com. (
                         2018040600     ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      midominio.com.
@       IN      A       10.194.100.48
